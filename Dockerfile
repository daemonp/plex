FROM ubuntu:16.04

ARG PLEX_TOKEN
ARG PLEX_PASS

RUN echo "force-unsafe-io" > /etc/dpkg/dpkg.cfg.d/02apt-speedup &&\
    echo "Acquire::http {No-Cache=True;};" > /etc/apt/apt.conf.d/no-cache && \
    apt-get -qq update && \
    apt-get -qqy dist-upgrade && \
    apt-get install -qqy \
      iproute2 \
      ca-certificates \
      ffmpeg \
      git \
      jq \
      openssl \
      xmlstarlet \
      curl \
      udev \
      sudo \
      wget \
    && \
    apt-get -y -qq autoremove && \
    apt-get -y -qq clean && \
    rm -rf /var/lib/apt/lists/* && \
    rm -rf /tmp/* && \
    useradd --system --uid 797 -M --shell /usr/sbin/nologin plex && \
    if [ "${PLEX_PASS}" = "true" ]; then PLEX_TYPE_FLAG="--token=${PLEX_TOKEN}" ; fi && \
    git clone --depth 1 https://github.com/mrworf/plexupdate.git /plexupdate && \
    /plexupdate/plexupdate.sh ${PLEX_TYPE_FLAG} -a -d  && \
    apt-get -qqy purge git &&\
    apt-get -qqy autoremove && \
    apt-get -qqy clean && \
    rm -rf /var/lib/apt/lists/* && \
    rm -rf /tmp/* && \
    mkdir -p /data/config 

VOLUME /data
USER plex
EXPOSE 32400

# the number of plugins that can run at the same time
ENV PLEX_MEDIA_SERVER_MAX_PLUGIN_PROCS 4

# ulimit -s $PLEX_MEDIA_SERVER_MAX_STACK_SIZE
ENV PLEX_MEDIA_SERVER_MAX_STACK_SIZE 3000

# location of configuration, default is
# "${HOME}/Library/Application Support"
ENV PLEX_MEDIA_SERVER_APPLICATION_SUPPORT_DIR /data/config

ENV PLEX_MEDIA_SERVER_HOME /usr/lib/plexmediaserver
ENV LD_LIBRARY_PATH /usr/lib/plexmediaserver
ENV TMPDIR /tmp

WORKDIR /usr/lib/plexmediaserver
CMD test -f /data/config/Plex\ Media\ Server/plexmediaserver.pid && rm -f /data/config/Plex\ Media\ Server/plexmediaserver.pid; \
    chown plex:plex /data/config ; \
    ulimit -s $PLEX_MAX_STACK_SIZE && ./Plex\ Media\ Server

